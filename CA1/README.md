## CA1 - Version Control with Git

## Operative Guidelines: <p>
1. Clone private repository to a local folder in the local machine with the command: <p>
```
 git clone git@bitbucket.org:mcarva/devops-21-22-atb-1211779.git
```

2. Create local folder with the name **CA1**.


3. Copy code of main application already added to the remote repository (Tutorial React.js and Spring Data REST application) 
into new folder.


4. Commit changes and push, associating tag v1.1.0 (initial version): <p>
```
 git add .
 git commit -m "CA1:  initial commit resolving issue #1" 
 git tag -a v1.1.0 -m "initial version" 
 git push origin master v1.1.0 
```
## First Week Assignement

Below are the requirements for the first week, divided in steps for better understanding.
No branches were used during this week assignment. The employment of git as a version control
system is highlighted in the following steps.

1. Add a new field to record the years of the employee in the company (e.g., jobYears). Validate changes with unit tests.

![jobYears](resources/jobYears.png)

2. Commit changes and push, associating tag v1.2.0 (second version): <p>
```
 git add .
 git commit -m "CA1:  add new field (jobYears) to Employee class resolving issue #2" 
 git tag -a v1.2.0 -m "second version"
 git push origin master v1.2.0
```

3. Mark end of assignament with the tag ca1-part1: <p>
```
 git tag -a ca1-part1 -m "end of first assignment"
 git push origin master ca1-part1
```
## Second Week Assignment

Below are the requirements for the second week, divided in steps for better understanding.
Branches were used during this week assignment. The employment of git as a version control
system is highlighted in the following steps.

1. Add a new field (e-mail) to the application, including support and testing. Develop this new feature in a new branch 
named "email-field": <p>
```
 git branch email-field
 git checkout email-field
 git add .
 git commit -m  "CA1: add new feature (email field) resolving issue #3"
 git push origin email-field
```

![emailField](resources/emailField.png)

2. Merge new implemented feature with the master branch, with a new tag **v.1.3.0**: <p>
```
 git checkout master 
 git tag -a v1.3.0 -m "new email field"
 git merge email-field
 git push origin master 
 git push origin master v1.3.0 -> adds tag to last commit pushed
 git branch -d email-field -> deletes the previously created branch. No longer needed after merging with master
```

3. Create a new branch to fix the new email-field. The server should only accept Employees
with a valid email (e.g., an email must have the "@" sign): <p>
```
 git branch fix-invalid-email 
 git checkout fix-invalid-email
 git add . 
 git commit -m  "CA1: fix new feature email field #4" 
 git push origin fix-invalid-email
```

![fixEmailField](resources/fixEmailField.png)

4. Merge new implemented feature with the master branch, with a new tag **v.1.3.1**: <p>
```
 git checkout master 
 git tag -a v1.3.1 -m "fix email field"
 git merge fix-invalid-email 
 git push origin master 
 git push origin master v1.3.1 
 git branch -d fix-invalid-email 
```

5. Mark end of assignament with the tag ca1-part2: <p>
```
 git add . 
 git commit -m "CA1: end of second assignment" 
 git tag -a ca1-part2 -m "end of second assignment" 
 git push origin master ca1-part2
```

![CommitList](resources/commitList.png)

## Alternative Solution - Mercurial

[Git vs Mercurial](https://www.javatpoint.com/git-vs-mercurial) <p>
[Mercurial vs. Git: How Are They Different?](https://www.perforce.com/blog/vcs/mercurial-vs-git-how-are-they-different)

Mercurial is an alternative version control system that is commonly used instead of Git. These are the most popular 
distributed version control systems in the market. Although quite similar, their differences make them ideal in different
use cases.

Just as Git, Mercurial is a free, open-source, distributed source control management tool. 
It can handle projects of any size and offers an easy and intuitive interface.
Currently, Mercurial is not a widely used development tool. Still, some software development giants
like Facebook, Mozilla, and World Wide Web Consortium use it.

To better understand their similarities and differences, a summary is presented below:

|                  | Git                                                                                                       | Mercurial                                                                                                                                                                                                                     |
|------------------|-----------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Complexity**   | More complex.                                                                                             | Simpler.                                                                                                                                                                                                                      |
| **Safety**       | Git offers many functions to enhance safety.                                                              | Safer for less experienced users.                                                                                                                                                                                             |
| **Branching**    | Git has a powerful and effective branching model. Branching in Git is better than branching in Mercurial. | Branching in Mercurial doesn't have the same meaning as in Git. Mercurial embeds the branches in the commits, where they are stored forever. This means that branches cannot be removed because that would alter the history. |
| **Staging Area** | Supports the staging area.                                                                                | There is no index or staging area before the commit in Mercurial.                                                                                                                                                             |
| **Maintenance**  | Needs periodic maintenance for repositories.                                                              | Does not require any maintenance.                                                                                                                                                                                             |
| **Performance**  | Slightly slower than Mercurial.                                                                           | Faster than Git.                                                                                                                                                                                                              |
| **Parents**      | Unlimited Parents                                                                                         | Only allows two parents.                                                                                                                                                                                                      |
| **Strength**     | Git has become an industry-standard, which means more developers are familiar with it.                    | Mercurial is easy to learn and use, which is useful for less-technical content contributors.                                                                                                                                  |


The biggest difference between Mercurial vs. Git is the branching structure. 
It can be argued that branching is better in Git than in Mercurial. 
Even though Mercurial may be easier to learn and use, its branching model often creates confusion.

On the other hand Mercurial’s syntax is simpler, and the documentation is easier to understand.
Furthermore, it works the way a tool should — you don’t think about it while using it.
Conversely, with Git, you might end up spending time figuring out finicky behavior and pouring over forums for help.

Once you get over the learning curve, Git offers teams more flexibility. 

## Alternative Implementation

The assignment implementation using Mercurial as a VCS was implemented using a different repository host. Bitbucket does not support 
Mercurial as a VCS. The HelixTeamHub repository was selected as a repository host for this alternative implementation.

Also, the alternative implementation will only use one file (README.md) to simulate the implementation of new features on
the application. The focus will be on the alternative commands using Mercurial as well as the major 
differences with Git. The application development was not replicated. Instead, a markdown file
(README.md) was used to simulate the application development and changes.

### Operative Guidelines: <p>


1. Create a local repository with a new file (README.md) and connect it with the remote repository:
```
echo "# Hello World" >> README.md
hg init
hg add README.md
hg commit -m "Initial commit"
hg push https://1211779isepipppt@helixteamhub.cloud/petite-shield-4180/projects/devops-21-22-atb-1211779/repositories/mercurial/devops-21-22-atb-1211779
```
Mercurial won’t remember the repository url and you have to type it each time a commit is pushed. This can be
fixed by creating a file into the hg folder .hg/hgrc containing the default path.

2. Make some changes in the README.md file and push, associating tag v1.1.0 (initial version): <p>
```
 hg commit -m "CA1:  initial commit resolving issue #1" 
 hg tag v1.1.0 -m "initial version" 
 hg push https://1211779isepipppt@helixteamhub.cloud/petite-shield-4180/projects/devops-21-22-atb-1211779/repositories/mercurial/devops-21-22-atb-1211779
```

A difference with Git is that we don’t need to add README.md before running the commit.
As long as we don’t add/remove any file, Mercurial will know what has changed since last commit.
This is convenient, as long as you want to commit every file you changed.

3. Add a new field (e-mail) to the README.md file. 
Develop this new feature in a new branch named "email-field":
```
 hg branch email-field
 hg commit -m "email-field" 
 hg tag v1.2.0 -m "second version" 
 hg push --new-branch https://1211779isepipppt@helixteamhub.cloud/petite-shield-4180/projects/devops-21-22-atb-1211779/repositories/mercurial/devops-21-22-atb-1211779
 hg update default 
 hg merge email-field
 hg push https://1211779isepipppt@helixteamhub.cloud/petite-shield-4180/projects/devops-21-22-atb-1211779/repositories/mercurial/devops-21-22-atb-1211779 
```

![MercurialLog](resources/mercurialLog.png)

In Mercurial, a branch is embedded in a commit.
A commit done in a ‘do-test’ branch will always remain in such a branch.
This means you cannot delete, or rename branches, because you would be changing the history of the commits on those branches.
You can ‘close’ branches though. 