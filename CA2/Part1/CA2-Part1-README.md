## CA2-Part1 - Build Tools with Gradle

## Operative Guidelines: <p>

1. For Part 1 of this assignment, it is necessary to download and commit to the private
repository (in a folder for Part 1 of CA2) the example application available at
   https://bitbucket.org/luisnogueira/gradle_basic_demo/: <p>
```
 git clone git@bitbucket.org:luisnogueira/gradle_basic_demo.git
```

The example application emulates a chat room server, allowing different clients looged on the same server 
to exchange direct messages.

## First Week Assignement
Below are the requirements for the first week, divided in steps for better understanding. The implementation and changes 
made to the Gradle script are highlighted below.

### 1. Add a new task to execute the server.

In order to maintain agreement with the runClient task already implemented in the Gradle script, the port 59001 was
chosen as the default port on this task.

![serverTask](resources/serverTask.png)

Running the task runServer with the gradle wrapper shows the chat successfully running. 

![serverTaskTerminal](resources/serverTaskTerminal.png)

### 2. Add a simple unit test and update the gradle script so that it is able to execute the test.

The provided code for the unit test can be seen below:

![unitTestCode](resources/unitTestCode.png)

In order to use the JUnit framework, the dependency must be added to the gradle script. If this is not done, the methods
dependent on this framework cannot be accessed.

![unitTestCodeNoDependencies](resources/unitTestNoDependencies.png)

To add the dependency in Gradle, the following code must be added to the script.

![unitTestGradle](resources/unitTestGradle.png)

The implemented unit test now recognizes the commands from the JUnit framework.

![unitTestCodeWithDependencies](resources/unitTestWithDependencies.png)


To check that the tests are successful, using the command **./gradlew test** all the tests in the project are runned.

![unitTestGradleRun](resources/unitTestGradleRun.png)

### 3. Add a new task of type Copy to be used to make a backup of the sources of the application. 

To implement this task, the following code was added to the Gradle script:

![backupTask](resources/backupTask.png)

This task of the type copy, copies all contents in the src folder and adds them into a new backup folder. If the 
folder already exists, the files are overwritten. However, if a class was deleted in the src, it will remain in the 
backup folder unless it is manually deleted.

<center>Before backup task run</center>

![beforeBackup](resources/beforeBackup.png)

<center>After backup task run</center>

![afterBackup](resources/afterBackup.png)


### 4. Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application

To implement this task, the following code was added to the Gradle script:

![zipTask](resources/zipTask.png)

This task of the type zip, archives all contents in the src folder and adds the into a zip file. 
Running the command **./gradlew zipArchive** a new src.zip is created.

![zipFile](resources/zipFile.png)

