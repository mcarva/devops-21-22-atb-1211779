# Maven; cloud deployment of Web+DB(H2) with Ansible (ISEP Cloud)

## INTRODUCTION

The proposed goal of this assignment is to implement a pipeline in Jenkins for our Project
Management System project. This particular assignment consisted in a pipeline to build, test and deploy the backend 
and frontend APIs to a cloud server (ISEP cloud server). H2 was used as a database solution and Maven as a build tool.

The following structure was used as a solution for this task:

![](./images/devopsDiagram.png)

A control virtual machine with the Jenkins pipeline fetches the repository located in Bitbucket with git.
It then configures, using Ansible, the dependencies of 3 different VMs in the Cloud server - a DB VM with an H2 database,
a WEB VM with Apache and a Backend with Tomcat. The project is built and tested
in the Jenkins pipeline. After the artifacts are built, they are deployed to the cloud servers
and the required services are started.

With this brief overview, the implementation details of this solution is explored in the next chapter.

## IMPLEMENTATION

### Control Virtual machine

The Control virtual machine has the purpose to configure and run the Jenkins pipeline for this assignment. A Vagrantfile
was used to create this VM.

````
Vagrant.configure("2") do |config|
    config.vm.box = "bento/ubuntu-20.04"
    config.vm.provision "shell", inline: <<-SHELL
      sudo apt-get update -y
      sudo apt-get install iputils-ping -y
      sudo apt-get install python3 --yes
      sudo apt-get install openjdk-11-jdk-headless -y
    SHELL

    ## control == 192.168.33.12

    config.vm.define "control" do |control|
      control.vm.box = "bento/ubuntu-20.04"
      control.vm.hostname = "control"
      control.vm.network "private_network", ip: "192.168.33.10"
      control.vm.provider "virtualbox" do |v2|
        v2.memory = 2048
      end

      # To run ansible from jenkins
      control.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=775,fmode=600"]

      # To access jenkins in port 8090
      control.vm.network "forwarded_port", guest: 8080, host: 8090
      control.vm.provision "shell", inline: <<-SHELL
      
      sudo apt-get update -y
      sudo apt-get install -y nano git
      sudo apt-get install -y --no-install-recommends apt-utils
      sudo apt-get install software-properties-common --yes
      
      # Install Node
      sudo apt-get install -y curl
      sudo apt-get install apt-transport-https
      curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
      sudo bash nodesource_setup.sh
      sudo apt install nodejs

      # Install Ansible
      sudo apt-add-repository --yes --u ppa:ansible/ansible
      sudo apt-get install ansible --yes

      # Install Jenkins
      sudo apt-get install -y avahi-daemon libnss-mdns
      sudo apt-get install -y unzip
      wget https://mirrors.jenkins.io/war-stable/latest/jenkins.war

      SHELL


      control.vm.provision "shell", :run => 'always', inline: <<-SHELL
       # Run Jenkins everytime the Virtual machine is started
         sudo java -jar jenkins.war &
       SHELL

    end
 end
````
To highlight in this file, the Control machine runs on an Ubuntu 20.04 box and has the 192.168.33.10 IPv4.
It is accessed in the host machine through port 8090.
On it, several dependencies are defined, including python3 (to run Ansible), Java 11, Node, Ansible and the jenkins.war executable.
Also, some utilitarian dependencies are added - ip utils ping to debug communications with the cloud VMs and others 
to help install the main dependencies.

Once the _vagrant up_ command is run and all the dependencies are installed, the jenkins.war executable is run (
as a background process - defined in the _**sudo java -jar jenkins.war &**_ line in Vagrantfile). 

Jenkins can now be accessed through the url http://192.168.33.10:8080/. A password is requested to unlock Jenkins (see below).
By creating a shell session in the Control VM (with the _**vagrant ssh control**_ command), the password can be found
by entering the following command:

````
sudo cat /root/.jenkins/secrets/initialAdminPassword
````

![](./images/jenkinsSignIn.png)

Apart from the suggested plugins by Jenkins, three other plugins must be added - Ansible plugin, The JaCoCo plugin
and Javadoc plugin.

Next, the pipeline is configured. The repository url is defined https://bitbucket.org/switch-2021/projectg5-devops/, 
as well as the credentials for this repository (app password in Bitbucket). Finally, the path to the jenkinsfile 
for this pipeline is configured.

![](./images/jenkinsCredentials.png)

![](./images/jenkinsPipelineConfiguration.png)

The jenkinsfile for this task is as follows:

````
pipeline {
    agent any

    stages {
        stage('Check out') {
            steps {
                echo 'CHECKING OUT...'
                git credentialsId: 'devops-task7', url: 'https://bitbucket.org/switch-2021/projectg5-devops/'
            }
        }
        stage('Setup cloud servers') {
            steps {
                echo 'SETTING UP CLOUD SERVERS...'
                ansiblePlaybook credentialsId: 'root', disableHostKeyChecking: true, inventory: 'DEVOPS/task7/hosts', playbook: 'DEVOPS/task7/playbook-config.yaml'
            }
        }
        stage('Change application.properties file') {
            steps {
                echo 'CHANGING APPLICATION PROPERTIES FILE...'
                sh 'sudo rm src/main/resources/application.properties'
                sh 'sudo cp DEVOPS/task7/application.properties src/main/resources/'
            }
        }
        stage('Change URL_API file') {
            steps {
                echo 'CHANGING BACKEND URL IN FRONTEND APP...'
                sh 'sudo rm webapp/src/services/URL_API.js'
                sh 'sudo cp DEVOPS/task7/URL_API.js webapp/src/services/'
            }
        }
        stage('Assemble') {
            steps {
                echo 'ASSEMBLING...'
                sh './mvnw package -Dmaven.test.skip=true'
            }
        }
        stage('Report') {
            steps {
                echo 'REPORTING...'
                sh './mvnw test jacoco:report'
                junit testResults: '*/surefire-reports/*.xml'
                jacoco exclusionPattern: '**/*Test*.class', execPattern: '**/jacoco.exec', inclusionPattern: '**/*.class'

                sh './mvnw javadoc:javadoc'
                javadoc javadocDir: 'target/site/apidocs', keepAll: false
            }
        }
        stage('Archive') {
            steps {
                echo 'ARCHIVING...'
                archiveArtifacts 'target/*.war'
            }
        }
        stage('Build Frontend') {
            steps {
                echo 'BUILDING FRONTEND...'
                dir('webapp') {
                    sh 'npm install'
                    sh 'sudo npm run build'
                }
            }
        }
        stage('Deploy to Cloud Servers') {
            steps {
                echo 'DEPLOYING TO CLOUD SERVERS...'
                ansiblePlaybook credentialsId: 'root', disableHostKeyChecking: true, inventory: 'DEVOPS/task7/hosts', playbook: 'DEVOPS/task7/playbook-run.yaml'
            }
        }
    }
}
````

10 different stages are defined in the jenkinsfile :
- **Check-out** : fetch the repository in Bitbucket
- **Setup cloud servers** : runs the Ansible playbook that installs the dependencies in the cloud servers
- **Change application.properties file** : change the app properties to reflect the cloud url where the database is running
- **Change URL_API file** : change the cloud url that the frontend app uses to fetch data from the backend API
- **Assemble** : assembles the backend API using Maven
- **Report** : runs and publishes the backend API tests and the jacoco reports
- **Archiving** : archives the assembled artifacts of the backend API
- **Build Frontend** : installs the frontend API dependencies using Node and builds the frontend artifacts
- **Deploy to Cloud Servers** : runs the Ansible playbook that deploys the created artifacts into the cloud servers

Aa described above, two different Ansible playbooks are used to configure and manage the cloud servers. The playbooks
are detailed in the Ansible playbooks chapter.

### Cloud Servers

As referenced before, 3 virtual machines where created in the DEI cloud servers - a DB VM,
a WEB VM a Backend VM.
![](./images/VMs.png)

All 3 machines start with a bare Ubuntu 20.04 LTS system.

Although the Control VM machine through Ansible in the Jenkins pipeline is responsible with installing the dependencies 
in each machine, in order to run the Ansible commands in each of these machines, python was installed before the first execution
of the pipeline.

To do this, in each of these machines the following commands were executed:

````
sudo apt-get update
sudo apt-get install python3 -y
````

The static network configuration is defined with the creation of each VM and can be consulted in the 
https://vs-ctl.dei.isep.ipp.pt/web. Below, this configuration is presented:

![](./images/DBIP.png)
![](./images/WEBIP.png)
![](./images/backendIP.png)




### Ansible playbooks

Before describing the Ansible playbooks used in this task, two Ansible configuration files must be described: the hosts file 
and the inventory file. The host file sets the IPs to the cloud servers and the ssh keys needed to connect to them. The 
inventory file (ansible.cfg) sets the name of the collection of hosts and where the host file can be found. This way, when
the playbooks are executed, Ansible knows how to connect to these machines. Below, both files can be seen:

<center><bold>Hosts</bold></center>

````
[servers]
web ansible_host=vsgate-ssh.dei.isep.ipp.pt ansible_ssh_port=10175 ansible_ssh_user=root ansible_ssh_pass=**********
db ansible_host=vsgate-ssh.dei.isep.ipp.pt ansible_ssh_port=10867 ansible_ssh_user=root ansible_ssh_pass=**********
backend ansible_host=vsgate-ssh.dei.isep.ipp.pt ansible_ssh_port=10891 ansible_ssh_user=root ansible_ssh_pass=**********
````

The ssh passwords to the VMs were hidden for security purposes. The ports defined in the hosts file respect the static
IPs referenced in the cloud servers chapter.

<center><bold>Inventory (ansible.cfg)</bold></center>

````
[defaults]
inventory = /vagrant/hosts
remote_user = root
````

Finally, the Ansible playbooks created. The first playbook to be explored is the playbook responsible
with installing the dependencies in the cloud servers.
The full playbook is as follows:

<center><bold>Configuration playbook</bold></center>

````
---
- hosts: servers
  become: yes
  tasks:
    - name: Update cache
      apt: update_cache=yes
    - name: Remove Apache
      apt: name=apache2 state=absent
    - name: Remove Tomcat9
      apt: name=tomcat9 state=absent
    - name: Update cache
      apt: update_cache=yes
    - name: Update Package Lists
      ansible.builtin.shell: sudo apt-get update
    - name: Install IP utils
      ansible.builtin.shell: sudo apt-get install iputils-ping -y
    - name: Install Python 3
      ansible.builtin.shell: sudo apt-get install python3 --yes 
    - name: Install Java
      ansible.builtin.shell: sudo apt-get install openjdk-11-jdk-headless -y


- hosts: db
  become: yes
  tasks:
    - name: Install H2
      get_url:      
      url:  https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
      dest: ./
    - name: Initialize H2
      ansible.builtin.shell: nohup java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -tcpPort 2223 -webPort 2224 -ifNotExists > ~/out.txt &

- hosts: web
  become: yes
  tasks:
    - name: Install Apache
      apt: name=apache2 state=present
    - name: Clean frontend
      ansible.builtin.file:
      path: /var/www/html/*
      state: absent

- hosts: backend
  become: yes
  tasks:
    - name: Install Tomcat 9
      apt: name=tomcat9 state=present
    - name: Clean Tomcat
      ansible.builtin.file:
      path: /var/lib/tomcat9/webapps/*
      state: absent
    
````

First, common dependencies are installed/removed in all cloud VMs - Tomcat and Apache are removed and 
IP utils (for debugging purposes), python 3 and Java 11 are installed. Even though Python should be installed
in each VM, the reference in the Ansible playbook serves as a precaution.

Then dependencies are defined for the DB virtual machine. In this machine the H2 database is fetched and initialized.
To highlight the _nohup_ argument in the H2 initialization which guarantees that the process continues running even after
the shell session is closed.

After the WEB virtual machine dependencies. Here where the frontend must be executed,
Apache2 is installed and the directory where the API is to be runned is cleaned.

Finally, the Backend dependencies. In this machine the backend API is executed. To do this, Tomcat9 is employed.
Like the WEB virtual machine, the directory where the backend API is to run is also cleaned.

<br>
The last playbook is the playbook responsible to deploy the backend and frontend APIs to their respective 
cloud virtual machines.

<bold><center>Run playbook</center></bold>


````
---
- hosts: servers
  become: yes
  tasks:
    - name: Update cache
      apt: update_cache=yes

- hosts: web
  become: yes
  gather_facts: false
  tasks:
    - name: Stop Apache Service
      ansible.builtin.shell: sudo service apache2 stop
    - name: Copy frontend build files
      copy: 
        src: ~/.jenkins/workspace/devops-task7/webapp/build/ 
        dest: /var/www/html/
    - name: Start Apache Service
      ansible.builtin.shell: sudo service apache2 start

- hosts: backend
  become: yes
  gather_facts: false
  tasks:
    - name: Stop Tomcat Service
      ansible.builtin.shell: sudo service tomcat9 stop
    - name: Copy war file from localhost
      copy:
        src: ~/.jenkins/workspace/devops-task7/target/switchproject-1.0-SNAPSHOT.war
        dest: /var/lib/tomcat9/webapps/
    - name: Start Tomcat Service
      ansible.builtin.shell: sudo service tomcat9 start


````

In the WEB virtual machine, the Apache service is stopped (as a precaution), the frontend artifacts built in Jenkins 
are copied into the Apache directory and the Apache service is started.

In the Backend virtual machine a similar approach is used. The Tomcat service is stopped (as a precaution),
the backend artifacts built in Jenkins are copied into the Tomcat directory and the Tomcat service is started.

This playbook ensures that the artifacts are deployed into the correct directories in the cloud virtual machines
so that the Apache and Tomcat services can provide access to the frontend and backend APIs respectively.

### Other configuration files

Besides the Ansible related files, two other files must be referenced. These files are part of the API files but must 
be altered to reflect the deployment associated with this task. These files are application.properties (backend API)
and URL_API.js (frontend API).

The application.properties file configures the database that is used by the backend API. Therefore, given that the 
database for this task is located in the DB cloud server, the original file must be changed prior to the project
assembly. The file is described below:

![](./images/appProperties.png)

Important in this file is the datasource url (line 4). This url (10.9.23.99:2223) matches the IPv4 (10.9.23.99) and 
port (2223) where the H2 database in the DB cloud virtual machine is running.

On the other hand, the URL_API.js defines the backend url used by the frontend. In the frontend API, all backend 
requests use this base URL. Since the backend is running on the Backend virtual machine in the cloud server,
the original url used must be changed.

![](./images/URL_API.png)

The url matches the url (http://vs891.dei.isep.ipp.pt) and port (8080) where the Tomcat service is being run.

### Running the Jenkins pipeline

With all the configuration files properly detailed, we can now run the Jenkins pipeline. Below are the results of 
one build in Jenkins - including the stages, a build overview and the javadocs produced.

![](./images/builds.png)
![](./images/build.png)
![](./images/jacoco.png)

The frontend can now be accessed through the url http://vs175.dei.isep.ipp.pt/, as defined in the static network configuration
for the web virtual machine.

![](./images/frontend.png)
![](./images/frontend1.png)


## Final considerations / Conclusion

During the course of finding a solution for this task, several challenges were presented. Difficulties configuring the
hosts file (how could Ansible reach these machines through ssh connections), with running the Ansible 
playbooks in the cloud servers, with presenting the frontend and the connection between the frontend and backend APIs 
were some of the challenges that were faced.

Among those enunciated, the last two presented were the most difficult to overcome. Firstly, to run the Ansible playbooks
on the cloud servers, since these machines where bare boxes with Ubuntu 20.04, several Ansible configurations where tried
in order to force the installation of python in these machines - this was to be the first step in the Ansible playbook .
These tries failed and the solution achieved was to install manually python3 in each cloud virtual machine.

Secondly, the first solution to the cloud servers was to have only two VMs - one DB and one WEB/Backend with both the 
backend and frontend APIs. Ansible would install Apache and Tomcat in the WEB/Backend virtual machines and start the services.
The deployment was successfully achieved but the frontend API could not reach the backend API. This was due to the fact 
that since both machines were running in the same virtual machine (same origin), CORS would block the requests between both APIs.
Several tries were made to allow cross origin requests to go through. These included changes to the backend API (Spring 
boot configurations) and changes to the configuration file of Tomcat. These tries were unsuccessful. The solution, 
as presented in this document, was to separate both applications and create a new virtual machine. The frontend API and
backend API were separated and since the requests from the frontend were from another origin, these were not blocked
by CORS.

Despite the many challenges, this assignment was very interesting and enriching since it encompassed the many technologies
that were addressed in the semester. It also gave the possibility to explore and see the many capabilities of
using Ansible which was not yet explored in this course.

Ansible allowed to manage the dependencies on several machines and the deployment of our applications. By using a very simple
language (yaml) it allowed complex configurations to be managed in a simple and direct way.

#### Analysis of the alternatives

#### Maven *vs* Gradle Comparison

The following is a summary of the pros and cons of Apache Maven and Gradle:

A comparative table between the two build tools can be seen below:

|                  | Gradle                                                                                      | Maven                                                                                             |
|------------------|---------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| **Based on**     | Based on developing domain-specific language projects.                                      | Based on developing pure Java language-based software.                                            |
| **Configuration** | It uses a Groovy-based Domain-specific language(DSL) for creating project structure.        | It uses Extensible Markup Language(XML) for creating project structure.                           |
| **Focuses on**   | Developing applications by adding new features to them.                                     | Developing applications in a given time limit.                                                    |
| **Performance**  | It performs better than maven as it optimized for tracking only current running task.       | It does not create local temporary files during software creation, and is hence – slower.         |
| **Java Compilation**  | It avoids compilation.                                                                      | It is necessary to compile.                                                                       |
| **Usability**  | It is a relatively new tool, which requires users to spend a lot of time to get used to it. | Older than Gradle and therefore this tool is a known tool for many users and is easily available. |
| **Customization**      | This tool is highly customizable as it supports a variety of IDE’s.                                                                          | This tool serves a limited amount of developers and is not that customizable.                     |
| **Languages supported**     | It supports software development in Java, C, C++, and Groovy.      | It supports software development in Java, Scala, C#, and Ruby.                                    |


Despite using Maven in this task, I prefer Gradle to Maven, as it allows more customization and is overall simpler to use.

#### Containers *vs* Virtual Machines

The key differentiator between containers and virtual machines is that <b>virtual machines virtualize an entire machine down to the hardware layers</b> and
<b>containers only virtualize software layers above the operating system level</b>.

Despite only virtual machines were used in this task, both solutions were used in this project.

The following is a summary of the pros and cons of virtual machines and containers:

![img.png](images/img.png)

In our project context, I prefer virtual machines to containers. Despite Virtual machines are 
time consuming to build and regenerate, in my experience they are easier to configure and since they are not
ephemeral, in this task they were easier to reuse day after day.

#### PostgreSQL *vs* H2 Database

PostgreSQL is an open source object-relational database system that uses and extends the SQL language combined with many features that safely store
and scale the most complicated data workloads.

PostgreSQL is considered one of the most advanced open source relational database, 
with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance.

H2 is an open-source lightweight Java database. It can be embedded in Java applications or run in the client-server mode.
Mainly, H2 database can be configured to run as in memory database, which means that data will not persist on the disk.

Because of embedded database it is not used for production development, but mostly used for development and testing.

In this academic context and the easiness to install and run, I believe that the H2 Database is a better solution. 
For a web application in production, I would choose PostgreSQL.
