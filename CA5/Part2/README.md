## CA5-Part2 - CI/CD Pipelines with Jenkins

## Operative Guidelines: <p>

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot
application, gradle "basic" version (developed in CA2, Part2)
available at https://bitbucket.org/mcarva/devops-21-22-atb-1211779.

## Second Week Assignement

**1. Create a pipeline for the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2), using
Jenkins as a continuous integration system.**

For this week assginement the jenkinsfile must include the following stages:

* **Checkout** - checkout the code from the repository
* **Assemble** - compile and produce the archive files with the application.
* **Test** - execute the unit tests and publish in Jenkins the test results.
* **Javadoc** - generate the javadoc of the project and publish it in Jenkins.
* **Archive** - archive in Jenkins the archive files (generated during Assemble, i.e., the war file)
* **Publish Image** - Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

With these guidelines, the following Jenkinsfile was produced. It is similar to the previous assignment, apart from two
new stages that where added to the pipeline. Therefore, it is important to highligth:

* Javadoc stage - the Javadoc plugin was added to Jenkins, allowing the use of the javadoc command
  (_javadoc javadocDir: 'build/docs/javadoc', keepAll: true_) to publish the Javadoc produced with the
  _./gradlew javadoc_ command.
* Docker Image stage - the Docker Pipeline plugin was added to Jenkins. A dockerfile with the Tomcat image to produce 
a copy of the generated war file was added to the CA2-Part2 directory. With the Docker Hub credentials setup in Jenkins,
the commands used in the jenkinsfile allow the docker container to be created and pushed to a private repository
in Docker Hub with the number of the build attached to the image name.

![jenkinsFileNotepad](resources/jenkinsFile.png)

With the jenkinsfile created,  new Jenkins pipeline (ca5-part2-jenkinspipeline). This
setup can be seen below. Of note is the script path which indicates where the jenkinsfile is located and the repository
url.

![jenkinsPipelineConfiguration](resources/jenkinsPipelineConfiguration.png)

Now, we can run the pipeline in Jenkins by clicking the _Build Now_ command. Jenkins will fetch the repository,
build the application, test and publish results, archive the build and create and push the docker image.
Below the stages can be seen as well as the build results.

![jenkinsPipeline](resources/jenkinsBuilds.png)

![jenkinsBuild](resources/jenkinsBuild.png)

Below are the before mentioned dockerfile and the image that was pushed to DockerHub.

![dockerFile](resources/dockerFile.png)

![dockerHub](resources/dockerHub.png)


## Alternative Solution - Buddy

https://hackernoon.com/buddy-vs-jenkins-mt4v32u2

There are many continuous integration tools like Jenkins available in the market. The one explored in this assignment
was Buddy. Although you can use Buddy for free, there are limitations.
The main differences between both solutions can be seen below:

![buddy_jenkins](resources/buddy_jenkins.png)

Buddy is recent solution to CI problems, in contrast to Jenkins which is one of the first solutions. Buddy's 
user experience is much more enjoyable than Jenkins, and for people who don't have a lot of experience working
with these tools the learning curve is a lot smoother than Jenkins. On the other hand, customization is limited.
Since Buddy is a managed offering, Buddy comes prepackaged with a plethora of integrations that have been developed
in house and therefore plugins can't be added and modified.

Buddy also allows for the pipelines to written in a buddy.yaml file, an approach similar to jenkinsfile in Jenkins.

### Buddy Implementation

To implement the Buddy solution to this week's assignment, an account was created and a new pipeline was defined
for pur project.

![buddy_jenkins](resources/buudyHomePage.png)

Similar to our jenkinsfile, severak tasks were created. This process was smoother as the plugins needed for our 
project where easier to work with in this platform. The subsequent pipeline created and definition of actions can be seen
below.

![buddy_jenkins](resources/buddyPipeline.png)

![buddy_jenkins](resources/buddyAssemble.png)

![buddy_jenkins](resources/buddyTest.png)

![buddy_jenkins](resources/buddyJavadoc.png)

![buddy_jenkins](resources/buddyArchive.png)

![buddy_jenkins](resources/buddyDockerBuild.png)


After running the pipeline with the _Run_ command, the newly created Docker image was pushed to Docker Hub
and can be seen below in the Docker Hub repository.

![buddy_jenkins](resources/buddyDockerHub.png)













