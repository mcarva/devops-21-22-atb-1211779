pipeline {
 agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://mcarva@bitbucket.org/mcarva/devops-21-22-atb-1211779'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
				dir('CA2/Part2/react-and-spring-data-rest-basic') {
					bat './gradlew assemble'
				}
            }
        }
        stage('Test') {
            steps {
				echo 'Testing...'
				dir('CA2/Part2/react-and-spring-data-rest-basic') {
					bat './gradlew test'
					junit 'build/test-results/test/*.xml'
				}
            }
        }
        stage('Javadoc') {
                    steps {
        				echo 'Publishing Javadoc...'
        				dir('CA2/Part2/react-and-spring-data-rest-basic') {
        				 bat './gradlew javadoc'
        				 javadoc javadocDir: 'build/docs/javadoc', keepAll: true
        				}
                    }
                }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
				dir('CA2/Part2/react-and-spring-data-rest-basic') {
					archiveArtifacts 'build/libs/*'
				}
            }
        }
        stage ('Docker Image') {
            steps {
                 script {
                    docker.withRegistry('', 'mpc-dockerHub') {
                        def app = docker.build("mcarvalheiro/ca4pt1:jenkins-image-${env.BUILD_ID}", 'CA2/Part2/react-and-spring-data-rest-basic')
                        app.push()
                    }
                 }
            }
        }
    }
 }