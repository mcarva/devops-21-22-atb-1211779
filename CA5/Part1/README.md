## CA5-Part1 - Pipelines with Jenkins

## Operative Guidelines: <p>

The goal of the Part 1 of this assignment is to practice with Jenkins, using the "gradle
basic demo" project from CA2-Part1 available at https://bitbucket.org/mcarva/devops-21-22-atb-1211779.

## First Week Assignement

**1. Create a pipeline for the "gradle basic demo" project, 
using Jenkins as a continuous integration system.**

Jenkinsfile is a text file that contains the definition of a Jenkins Pipeline
and is checked into source control. For this assignment, four stages were defined:

* **Checkout** - To checkout the code from the repository;
* **Assemble** - Compiles and produces the archive files with the application.
* **Test** - Executes the unit tests and publish in Jenkins the test results.
* **Archive** - Archives in Jenkins the archive files (generated during Assemble)

With these guidelines, the following Jenkinsfile was produced. It is important to highlight:
* the definition of the url of the git repository (git 'https://mcarva@bitbucket.org/mcarva/devops-21-22-atb-1211779')
in the checkout stage;
* the _dir_ command in the assemble, test and archive stages, which sets the path where our "gradle basic demo" project
was implemented;
* the bat command to execute external commands. Since our pipeline runs on MS Windows, the bat command
must be used instead of the _sh_ command from Unix/Linux.

Since our repository is public, no SSH credentials were used in this assignement.

![jenkinsFileNotepad](resources/jenkinsFileNotepad.png)

With the jenkinsfile created we must now configure in Jenkins the Jenkins pipeline. This setup can be seen below. 
Of note is the script path which indicates where the jenkinsfile is located and the repository url.

![jenkinsPipelineConfiguration](resources/jenkinsPipelineConfiguration.png)

Now, we can run the pipeline in Jenkins by clicking the _Build Now_ command. Jenkins will fetch the repository, 
build the application, test and publish results and archive the build. Below the stages can be seen as well
as the build results.

![jenkinsPipeline](resources/jenkinsPipeline.png)

![jenkinsBuild](resources/jenkinsBuild.png)




