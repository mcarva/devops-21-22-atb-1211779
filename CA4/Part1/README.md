## CA4-Part1 - Containers with Docker

## Operative Guidelines: <p>

The goal of the Part 1 of this assignment is practice with Docker, creating docker
images and running containers using the chat application from CA2 available at 
https://bitbucket.org/luisnogueira/gradle_basic_demo/.

## First Week Assignement

**1. Create a docker image (i.e., by using a Dockerfile) to execute the chat
server.**

Docker can build images automatically by reading the instructions from a Dockerfile.
A Dockerfile is a text document that contains all the commands a user could call on the command line to
assemble an image.
Using docker build users can create an automated build that executes several command-line instructions in succession.

The docker build command builds an image from a Dockerfile and a context.

Two Dockerfile versions will be used to create our containers. One version where the 
chat server app is built "inside" the Dockerfile. The other version where the chat server app
is built on the host machine and the executable jar is copied from the 
host in the moment of creation of the container.





> **1. Chat server built "inside" the Dockerfile**
   
In this version the dockerfile takes the following shape:
![dockerFile1](resources/dockerfile.png)

First, the Ubuntu version to be used in the Container is defined with the _FROM_ command - in this case
ubuntu:18.04. After, the required dependencies for the container are defined (among these the java
version and git).

Afterwards, the clone command for our private repository is defined. The working directory is set to
where the chat server app source code is present. Then, the build command can be executed, so long as 
the _gradlew_ file as the execution permission.

Finally, the container communication port is set to 59001 and the chat server is executed 
with the _./gradlew runServer_ command. The CMD command only is executed when the docker 
container is started.

With the dockerfile created, we need to run the following command in the host terminal to 
build the container with the dockerfile instructions.

````
docker build . -t chat_docker_v1
````

To start the container the command below is used. The communication ports between host:guest are 
defined:

````
docker run -p 59001:59001 -d chat_docker_v1
````

In the host machine, using the command _./gradlew runClient_ in the chat server app source
code directory starts the chat application, that is linked with the server running in our container.

![runClient](resources/runClient.png)

>   **2.** **Chat server built in the host computer and copy the jar file "into" the Dockerfile**

To implement the second version, we must first stop the running container. To do this, we must 
get the containerID of the previously created container and stop it afterwards. These steps and 
associated commands are described below.

![dockerStop](resources/dockerStop.png)

The new dockerfile for this version is extremely simple. Another docker image with
the required dependencies is pulled from DockerHub (openjdk:11). A new directory
(/root) is created and defined as the working directory. The executable jar 
file is copied into the container (the jar file should be in the same directory as
the docker file). The same communication port as the previous version is exposed and finally 
the chat Server command to be executed when the container is started is also defined.

![dockerFile2](resources/dockerfileV2.png)

![jarFile](resources/jarFile.png)

The commands to build and run the new container are very similar to the previous version.
The only difference is the name of the image, which in this case is _chat_docker_v2_.

````
docker build . -t chat_docker_v2
docker run -p 59001:59001 -d chat_docker_v2
````

Again, in the host machine, using the command _./gradlew runClient_ in the chat server app source
code directory starts the chat application.

   **2. Tag the images and publish them in docker hub**

With an account created in DockerHub, we can publish the images previously created and 
save their state. A new repository was created in DockerHub in order to push the new 
images.

![dockerHubRepo](resources/dockerHubRepo.png)

To push our images, we must first login to DockerHub in the terminal. Then, new tags 
must be added to the images, to reflect the repository in DockerHub (mcarvalheiro/ca4pt1). This is required 
to push the images to the repository. Only after this can the images be pushed.

![firstImagePush](resources/firstImagePush.png)

![secondImagePush](resources/secondImagePush.png)

In DockerHub, we can now see the created images.

![dockerHubImages](resources/dockerHubImages.png)

