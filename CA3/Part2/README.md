## CA3-Part1 - Introduction to Virtualization

## Operative Guidelines: <p>

The goal of the Part 2 of this assignment is to use Vagrant to setup a virtual environment
to execute the tutorial spring boot application, gradle "basic" version (developed in
CA2, Part2).

Vagrant is an open-source software that helps us automate the setup of one or more VMs, using a simple
configuration file. It can run in several operating systems and supports different providers (i.e.
hypervisors, such as VirtualBox, KVM, VMWare, ...), depending on the OS.

Vagrant makes it easier to reproduce environments: sharing a VM becomes sharing a
vagrant configuration file

The Vagrant file available
at https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ will be used as an initial solution.

## Second Week Assignement

1. Copy the Vagrant file available at  https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/
, to the private repository (inside the folder for this assignment).


2. Update the Vagrantfile configuration so that it uses the gradle version of the
   spring application created in CA2-Part2.

To successfully run the Vagrant configuration file and create a new VM, the private repository must be
made public. This is necessary given the fact that when running git clone of a private repository, git will ask
for authentication using a prompt for reading the password. This prompt will break the provision script, since it should
execute without any user interaction. The simple way to avoid this is to make public the private repository.

In order to run the spring application created in CA2-Part2 we must make a few changes in the configuration files 
of the application. But first, we must understand how the Vagrant file is structured.

The Vagrantfile defines two different virtual machines - the <i>db</i> virtual machine and the <i>web</i> virtual machine.
The <i>db</i> virtual machine will host the database used by the web application, which is defined in the <i>web</i> 
virtual machine.

Therefore, Vagrantfile starts by defining the Vagrant box that will be used in both virtual machines (envimation)
ant the provisions that are common to both virtual machines (image below). 

![commonConfiguration](resources/commonConfigurations.png)

After the common configurations, the individual virtual machines are defined. For the <i>db</i> virtual machine,
a static IP is defined (192.168.56.11) as well as the ports were the H2 database will be available. The specific
provisions for this virtual machine are also defined. 

The definition of the provider was also added to the Vagrantfile in order to force the use of the Virtual Box as a 
virtualization tool for the defined Vagrant box. This prevents that the virtual machine starts using the 
VMware desktop as a provider (see alternative implementation).

![dbConfiguration](resources/dbConfiguration.png)

Finally, the <i>web</i> virtual machine is configured. A static IP is defined (192.168.56.10) 
as well as the ports where the web application is available. Also, more RAM memory is defined for this virtual machine
Like the <i>db</i> virtual machine the specific provisions for this virtual machine are defined. 

Like the previous virtual machine, the provider is configured to use the Virtual box as a virtualization tool.

![webConfiguration](resources/webConfigurations.png)

Some changes were made to the provisions for the <i>web</i> virtual machine. Among these, the location of 
the now public repository which stores the application is defined with:

````
git clone https://mcarva@bitbucket.org/mcarva/devops-21-22-atb-1211779.git
````

<br/><br/>
3. Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application 
uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.
<br/><br/>

Now that the Vagrantfile is configured, the Spring application created in CA2-Part2 must be changed in order to use
the H2 database and link with the <i>db</i> virtual machine using the static IP in the Vagrantfile (192.168.56.11).

The changes are described in the commits of the https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ repository. 
Among the changes we can highlight the files build.gradle (where the dependencies are defined), app.js (with the path
of the employees table), and more importantly the application.properties (where the database link to the <i>db</i> virtual machine
and console are configured).

![buildGradle](resources/buildGradle.png)

![appJS](resources/appJS.png)

![applicationProperties](resources/applicationProperties.png)

After pushing the changes made to the spring application to the public repository, the Vagrantfile can now be executed
with the command:

````
vagrant up
````

![vagrantUp](resources/vagrantUp.png)

Now we can access the web application using the url: http://localhost:8080/basic-0.0.1-SNAPSHOT/ or 
http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/ .

![reactApplication](resources/reactApplication.png)

With the configurations done in the application.properties file, the database console is also accessible through the
url: http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console . Using the defined username and password
the data stored in the database can now be seen and manipulated mannually.

![h2Console](resources/h2ConsoleWeb.png)


## Alternative Implementation

For this assignment the alternative to explore is an alternative technological solution for the
virtualization tool (i.e., a hypervisor alternative to VirtualBox). 

One alternative, using the same vagrant box (envimation/ubuntu-xenial), is to use VMware. This choice was made as the
vagrant box already employed supports both hypervisors.


![vagrantEnvimation](resources/envimationVMWare.png)


Contrary to Virtual Box, which is supported by default by the Vagrant software,
in order to use the VMware as a virtualization tool, besides having to install the VMware product, 
we must add the required plugins to the Vagrant software prior to using the provider.

### VMware vs Virtual Box


![VMware_Vbox](resources/VMware_Vbox.png)
<p align = "center">
VMware vsVirtualBox comparison https://phoenixnap.com/kb/virtualbox-vs-vmware 
</p>

The most obvious distinction between VMware and VirtualBox is that VirtualBox is 
a free and open-source software whereas
VMware is solely available for personal use. The free version of VMware for personal and educational use offers limited
functionality. Clones and snapshots, for example, are not supported. VMware also does not provide software virtualization,
although VirtualBox does.

VirtualBox supports a variety of virtual disc types (VMDK, VHD, HDD, and QED), as well as integration tools like Vagrant
and Docker whereas VMware does not support a large number of different disc formats. 

On the other hand, virtual machines created by VMware
are faster than those created by VirtualBox. 
On a smaller scale, this difference may not be impactful, but it could have 
a significant influence on an enterprise level.

### Implementation

In order to implement this week assignment using VMware, 
following the steps described in https://www.vagrantup.com/docs/providers/vmware/installation,
first we must install the Vagrant VMware Utility available in the link above. 

After, the following command must be executed in a terminal:

````
vagrant plugin install vagrant-vmware-desktop
````


Also, we must install this hypervisor on the host machine (available at 
https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html).






To use the IPs already defined in the Vagrant file, the network adapter previously configured in Virtual Box (image below),
was changed from 192.168.56.1 to 192.168.55.1 . This was done because the VMware couldn't use the static IPs
in the same network as the network adapter of Virtual Box - the IPs already configured in the 
Vagrant file (db: 192.168.56.11 and web: 192.168.56.10) would collide with the network used by the VirtualBox.

![virtualBoxNetwork](resources/virtualBoxNetworkAdapter.png)

![networkAdapterError](resources/networkAdapterError.png)

A few changes were also made to the Vagrant file. These were to force the virtual machines (db and web) to use the 
VMware as a provider for the virtualization of the Vagrant box.

![vmWareDB](resources/vmWareDB.png)

![vmWareWeb](resources/vmWareWEB.png)

Noteworthy is the fact that the private networks defined in the Vagrantfile are added to the list of networks
available to the VM but are not defined as the main IP for these machines. Therefore, the application can't be accessed
with the url http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/ because the main IP is automatically generated by the 
VM. However, the application can still be accessed through the link http://localhost:8080/basic-0.0.1-SNAPSHOT/.

![localHost](resources/localHost.png)