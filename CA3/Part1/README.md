## CA3-Part1 - Introduction to Virtualization

## Operative Guidelines: <p>

The goal of the Part 1 of this assignment is to practice with VirtualBox using the same
projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.

To do this, we have to first create the virtual machine as described in
*Lecture 05 - Introduction to Virtualization*.

The virtualization software used in this assignment was  VirtualBox, a free hypervisor
that runs in many host operating systems, such as Windows, Linux and OSX.

The environment of this software can be seen below:

![virtualBoxWorkEnvironment](resources/virtualBoxWorkEnvironment.png)

The selected OS for this assignement was Ubuntu 18.04 "Bionic Beaver" 64-bit.

As described in *Lecture 05 - Introduction to Virtualization*, the IP the Host and VM were connected
to the same network using the same network adapter. Later, with network-tools installed on the VM,
the network configuration file was edited, with the IP being setuped as **192.168.56.5/24**.

![virtualBoxHostManager](resources/virtualBoxHostManager.png)

![ipConfig](resources/ipConfig.png)

Other tools were installed in our VM, such as:
- ssh to open secure terminal sessions to the VM (from other hosts);
- ftp server so that a FTP protocol can be used to transfers files to/from the VM (from other hosts);
- git to download directly the necessary repositories for this assignment;
- java sdk 11 to run applications.

Also, the Filezila software was installed in the Host. Since the FTP server is enabled in the VM
a ftp application, like FileZilla, can now be used to easily transfer files to/from the VM. 

![fileZilla](resources/fileZilla.png)

With these extensive configurations steps concluded, the VM was finally ready to be used with the aspired
goal of this week assignement.

## First Week Assignement

With the VM configuration concluded, the next step for this week assignement was to 
clone the individual repository inside the VM. Therefore, inside the VM the following git command was used:

```
 git clone https://mcarva@bitbucket.org/mcarva/devops-21-22-atb-1211779.git
```

![gitClone](resources/gitClone.png)

Now, we can test the same projects from the previous assignments inside the VirtualBox VM.

### Spring-Boot tutorial (Maven)

To run the basic Spring-Boot tutorial (Maven) inside our VM, the following commands were used:

```
 $ cd devops-21-22-atb-1211779/CA2/Part2/alternative-implementation-maven/tut-react-and-spring-data-rest/basic
 $ chmod u+x mvnw
 $ ./mvnw spring-boot:run
```

The command **chmod u+x mvnw** was required, as the mvnw file didn't have the execute permission associated. Therefore,
when we wanted to execute the command to run the spring boot application it failed. Only by changing the permision was it
possible to run this command.

![permissionDenied](resources/permissionDenied.png)

The Maven command downloads the required dependencies and executes the Spring project. In the Host machine,
we can now see the frontEnd of the application using the path http://192.168.56.5:8080/ (IP address
of the VM and the port where the application is being shared).

![mavenApp](resources/mavenApp.png)

We have successfully created a web application running on a VM and available in the Host browser.

### Spring-Boot tutorial (Gradle)

To run the basic Spring-Boot tutorial (Gradle) inside our VM, from the root folder, the following commands were used:

```
 $ cd devops-21-22-atb-1211779/CA2/Part2/react-and-spring-data-rest-basic
 $ chmod u+x gradlew
 $ ./gradlew build
 $ ./gradlew bootRun
```
Just as explained in the previous Maven project, the command **chmod u+x gradlew** was required, as the gradlew file didn't
have the execute permission associated.

The *build* Gradle command downloads the required dependencies. The *bootRun* and executes the Spring project.
As expected, in the Host machine, we can now see the frontEnd of the application using the same path
as before http://192.168.56.5:8080/ . 

![mavenApp](resources/mavenApp.png)

We have successfully created a web application running on a VM and available in the Host browser.

### Gradle Basic Demo (Chat application)

To run the Server in the Gradle Basic Demo (Chat application) inside our VM, from the root folder, 
the following commands were used:

```
 $ cd devops-21-22-atb-1211779/CA2/Part1/gradle_basic_demo
 $ chmod u+x gradlew
 $ ./gradlew build
 $ ./gradlew runServer
```

A few issues ocurred when trying to run the applcation. Firstly, the *build* command couldn't execute as the 
*gradle-wrapper.jar* file was missing in the */gradle/wrapper* directory. Using the Filezilla software, the *gradle-wrapper.jar* 
file was mannually copied from the Host and past into the respective directory in the VM.

![jarError](resources/jarError.png)

Secondly, as the VM doesn't have a graphical envorinment, running the client task inside the VM results in an exception
as no graphics display could be found and launched.

![runClientFail](resources/runClientFail.png)

The solution is to run the server inside our VM, and running the client in the Host machine. For this to work, the runClient
task in the build.gradle file must be changed, as the IP address of our server is now the IP of the VM
(192.168.56.5) instead of the local Host machine.

![runClientIntelliJ](resources/runClientIntelliJ.png)

Now, executing first the command *./gradlew runServer* inside our VM and after by using the command *./gradlew runClient*
in our Host machine, the application can now successfully run.

<center> <b>Virtual Machine [Guest] </b> </center>

![runServer](resources/runServer.png)

<center> <b>Host</b> </center>

![runClientHost](resources/runClientHost.png)